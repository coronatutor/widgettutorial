-----------------------------------------------------------------------------------------
--
-- main.lua
-- 本練習重點在於如何使用Widget中的newButton API，如需更多資訊請參考API網址如下
-- https://docs.coronalabs.com/api/library/widget/newButton.html
--
-- 1.白色黃字為提示，請刪除後改成正確內容
-- 2.??????為填空，請刪除後改成正確內容
-- Author: Zack Lin
-- Time:2015/5/6
-----------------------------------------------------------------------------------------

--=======================================================================================
--引入各種函式庫
--=======================================================================================
display.setStatusBar( display.HiddenStatusBar )

local widget = require(請填入widget，用以匯入widget library，格式為字串)
--=======================================================================================
--宣告各種變數
--=======================================================================================
_SCREEN = {
	WIDTH = display.viewableContentWidth,
	HEIGHT = display.viewableContentHeight
}
_SCREEN.CENTER = {
	X = display.contentCenterX,
	Y = display.contentCenterY
}

local label
local btn1
local btn2

local initial
local handleButtonEvent
--=======================================================================================
--宣告與定義main()函式
--=======================================================================================
local main = function (  )
	initial()
end

--=======================================================================================
--定義其他函式
--=======================================================================================
initial = function (  )
	label = display.newText( "別亂按", 0,  0, system.nativeFont, 40 )
	label.x = _SCREEN.CENTER.X
	label.y = _SCREEN.CENTER.Y - 150

	btn1 = 請輸入widget library變數.newButton{
		id = "btn1",
		ivan = "me",
		zack = "you",
		width = 輸入寬度，例如200，格式為數字,
	    height = 輸入高度，例如100，格式為數字,
	    defaultFile = 輸入預設按鈕圖片路徑，例如images/btn_normal.png，格式為字串,
	    overFile = 輸入點擊後按鈕圖片路徑，例如images/btn_over.png，格式為字串,
	    label = 輸入按鈕文字，格式為字串,
	    fontSize = 30,
	    onEvent = 當按鈕事件觸發時所需呼叫的function，格式為函式變數
	}
	btn1.x = _SCREEN.CENTER.X
	btn1.y = _SCREEN.CENTER.Y + 100

	btn2 = widget.new請補完完整的函式名稱{
		id = "btn2",
		width = 80,
		height = 80,
		defaultFile = "images/Button1.png",
		overFile = "images/Button1Pressed.png",
		onEvent = handleButtonEvent
	}
	btn2.x = _SCREEN.CENTER.X
	btn2.y = ??????????????? + 300
end

handleButtonEvent = function ( event )
	if ("btn1" == event.target.id) then
		label.text = "你按了btn1"
	elseif ("btn2" == ???) then
		label.text = "你按了btn2"
	end
end
--=======================================================================================
--呼叫主函式
--=======================================================================================
main()